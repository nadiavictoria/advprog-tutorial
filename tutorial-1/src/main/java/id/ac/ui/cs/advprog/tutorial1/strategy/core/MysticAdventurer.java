package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
        //ToDo: Complete me

    public MysticAdventurer() {
        this.setAttackBehavior(new AttackWithMagic());
        this.setDefenseBehavior(new DefendWithShield());
    }

    @Override
    public String getAlias() {
        return "mystic adventurer";
    }
}
