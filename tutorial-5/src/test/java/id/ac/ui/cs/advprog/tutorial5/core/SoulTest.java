package id.ac.ui.cs.advprog.tutorial5.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SoulTest {

    Soul soul;

    @BeforeEach
    public void SetUp(){
        soul = new Soul(0, "Tati", 49, "F", "Mama");
        soul.setId(0);
    }

    @Test
    public void testGetID(){
        assertEquals(soul.getId(),0);
    }

    @Test
    public void testGetName(){
        assertEquals(soul.getName(),"Tati");
    }

    @Test
    public void testGetAge(){
        assertEquals(soul.getAge(),49);
    }

    @Test
    public void testGetGender(){
        assertEquals(soul.getGender(),"F");
    }

    @Test
    public void testGetOccupation(){
        assertEquals(soul.getOccupation(),"Mama");
    }
}
