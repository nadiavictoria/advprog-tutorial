package id.ac.ui.cs.advprog.tutorial5.service;

import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SoulServiceImplTest {
    @Mock
    private SoulRepository soulRepository;

    @InjectMocks
    private SoulServiceImpl soulService;

    private Soul soul;

    @BeforeEach
    public void setUp() {
        soul= new Soul(0, "Tati", 49, "F", "Mama");
    }

    @Test
    public void whenFindAllIsCalledItShouldCallSoulRepositoryFindAll(){
        soulService.findAll();
        verify(soulRepository,times(1)).findAll();
    }

    @Test
    public void whenFindSoulIsCalledItShouldCallSoulRepositoryFindSoul(){
        soulService.findSoul((long)0);
        verify(soulRepository,times(1)).findById((long)0);
    }

    @Test
    public void whenEraseIsCalledItShouldCallSoulRepositoryErase(){
        soulService.erase((long)0);
        verify(soulRepository,times(1)).deleteById((long)0);
    }

    @Test
    public void whenRewriteIsCalledItShouldCallSoulRepositoryAdd(){
        soulService.rewrite(soul);
        verify(soulRepository,times(1)).save(soul);
    }

    @Test
    public void whenRegisterIsCalledItShouldCallSoulRepositoryAdd(){
        soulService.register(soul);
        verify(soulRepository,times(1)).save(soul);
    }
}
