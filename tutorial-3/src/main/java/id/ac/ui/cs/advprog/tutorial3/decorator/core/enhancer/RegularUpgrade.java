package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {

    Random r = new Random();
    Weapon weapon;

    public RegularUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int add = r.nextInt(5)+1;
        return weapon.getWeaponValue() + add;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return weapon.getDescription()+" + Regular Upgrade";
    }
}
