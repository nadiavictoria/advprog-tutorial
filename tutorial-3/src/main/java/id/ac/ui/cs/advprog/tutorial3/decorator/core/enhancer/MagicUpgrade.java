package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class MagicUpgrade extends Weapon {

    Weapon weapon;
    Random r = new Random();

    public MagicUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 15-20 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int add = r.nextInt(6)+15;
        return weapon.getWeaponValue() + add;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return weapon.getDescription() + " + Magic Upgrade";
    }
}
