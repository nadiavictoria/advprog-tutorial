package id.ac.ui.cs.advprog.tutorial3.composite.service;

import id.ac.ui.cs.advprog.tutorial3.composite.core.Guild;
import id.ac.ui.cs.advprog.tutorial3.composite.core.Member;
import id.ac.ui.cs.advprog.tutorial3.composite.core.OrdinaryMember;
import id.ac.ui.cs.advprog.tutorial3.composite.core.PremiumMember;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuildServiceImpl implements GuildService {
    private Guild guild;

    public GuildServiceImpl() {
        Member master = new PremiumMember("Heathcliff", "Master");
        this.guild = new Guild(master);
    }

    @Override
    public void addMember(String parentNameRole, String childName, String childRole, String childType) {
        String[] nameRole = parentNameRole.split(" ");
        Member parent = guild.getMember(nameRole[0], nameRole[1]);
        Member child = childType.equals("premium") ? new PremiumMember(childName, childRole) :
                new OrdinaryMember(childName, childRole);
        guild.addMember(parent, child);
    }

    @Override
    public void removeMember(String parentNameRole, String childNameRole) {
        String[] parentNR = parentNameRole.split(" ");
        String[] childNR = childNameRole.split(" ");
        Member parent = guild.getMember(parentNR[0], parentNR[1]);
        Member child = guild.getMember(childNR[0], childNR[1]);
        guild.removeMember(parent, child);
    }

    @Override
    public List<Member> getMemberHierarchy() {
        return guild.getMemberHierarchy();
    }

    @Override
    public List<Member> getMemberList() {
        return guild.getMemberList();
    }
}
