package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
        //TODO: Complete me

    String name;
    String role;
    List<Member> childList;

    public PremiumMember(String name, String role) {
        this.name = name;
        this.role = role;
        childList = new ArrayList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public void addChildMember(Member member) {
        if (role.equals("Master")) childList.add(member);
        else if (this.childList.size()<3) childList.add(member);
    }

    @Override
    public void removeChildMember(Member member) {
        this.childList.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return childList;
    }
}
