package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;


import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Gun;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChaosUpgradeTest {

    private ChaosUpgrade chaosUpgrade;
    @BeforeEach
    public void setUp(){
        chaosUpgrade = new ChaosUpgrade(new Gun());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals("Gun", chaosUpgrade.getName());
    }

    @Test
    public void testGetMethodWeaponDescription(){
        //TODO: Complete me
        String sub = chaosUpgrade.getDescription().substring(chaosUpgrade.getDescription().length()-13);
        assertTrue(sub.equals("Chaos Upgrade"));
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int chaosVal = chaosUpgrade.getWeaponValue();
        int weapVal = chaosUpgrade.weapon.getWeaponValue();
        boolean bool = (chaosVal==weapVal+50)||(chaosVal==weapVal+51)||(chaosVal==weapVal+52)||(chaosVal==weapVal+53)||(chaosVal==weapVal+54)||(chaosVal==weapVal+55);
        assertTrue(bool);
    }

}
