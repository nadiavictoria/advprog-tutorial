package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MagicUpgradeTest {


    private MagicUpgrade magicUpgrade;

    @BeforeEach
    public void setUp(){
        magicUpgrade = new MagicUpgrade(new Longbow());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals(magicUpgrade.getName(), "Longbow");
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        String sub = magicUpgrade.getDescription().substring(magicUpgrade.getDescription().length()-13);
        assertEquals("Magic Upgrade", sub);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int magVal = magicUpgrade.getWeaponValue();
        int weapVal = magicUpgrade.weapon.getWeaponValue();
        boolean bool = (magVal==weapVal+15)||(magVal==weapVal+16)||(magVal==weapVal+17)||(magVal==weapVal+18)||(magVal==weapVal+19)||(magVal==weapVal+20);
        assertTrue(bool);
    }
}
