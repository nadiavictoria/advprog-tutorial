package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UniqueUpgradeTest {

    private UniqueUpgrade uniqueUpgrade;

    @BeforeEach
    public void setUp(){
        uniqueUpgrade = new UniqueUpgrade(new Shield());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals("Shield", uniqueUpgrade.getName());
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        String sub = uniqueUpgrade.getDescription().substring(uniqueUpgrade.getDescription().length()-14);
        assertEquals("Unique Upgrade", sub);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int unVal = uniqueUpgrade.getWeaponValue();
        int weapVal = uniqueUpgrade.weapon.getWeaponValue();
        boolean bool = (unVal==weapVal+10)||(unVal==weapVal+11)||(unVal==weapVal+12)||(unVal==weapVal+13)||(unVal==weapVal+14)||(unVal==weapVal+15);
        assertTrue(bool);

    }
}
