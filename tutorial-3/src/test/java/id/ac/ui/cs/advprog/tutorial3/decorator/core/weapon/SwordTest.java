package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SwordTest {

    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Sword();
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        String name = weapon.getName();
        assertEquals("Sword", name);
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        String desc = weapon.getDescription();
        assertEquals(desc, "Great Sword");
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int value = weapon.getWeaponValue();
        assertEquals(value, 25);
    }
}
