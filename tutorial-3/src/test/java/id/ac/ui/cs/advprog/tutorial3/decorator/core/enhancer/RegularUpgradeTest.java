package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Sword;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RegularUpgradeTest {

    private RegularUpgrade regularUpgrade;

    @BeforeEach
    public void setUp(){
        regularUpgrade = new RegularUpgrade(new Sword());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals("Sword",regularUpgrade.getName());
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        String sub = regularUpgrade.getDescription().substring(regularUpgrade.getDescription().length()-15);
        assertEquals("Regular Upgrade", sub);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int regVal = regularUpgrade.getWeaponValue();
        int weapVal = regularUpgrade.weapon.getWeaponValue();
        boolean bool = (regVal==weapVal+1)||(regVal==weapVal+2)||(regVal==weapVal+3)||(regVal==weapVal+4)||(regVal==weapVal+5);
        assertTrue(bool);
    }
}
