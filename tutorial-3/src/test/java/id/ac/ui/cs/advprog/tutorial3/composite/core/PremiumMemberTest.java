package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        String name = member.getName();
        assertEquals(name, "Wati");
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        String role = member.getRole();
        assertEquals(role, "Gold Merchant");
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member pucu = new OrdinaryMember("Pucu","Seer");
        member.addChildMember(pucu);
        assertEquals(member.getChildMembers().size(), 1);
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member pucu = new OrdinaryMember("Pucu","Seer");
        member.addChildMember(pucu);
        member.removeChildMember(pucu);
        assertEquals(member.getChildMembers().size(), 0);
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member puci = new OrdinaryMember("Pucu","Seer");
        Member puca = new OrdinaryMember("Pucu","Seer");
        Member puco = new OrdinaryMember("Pucu","Seer");
        Member puce = new OrdinaryMember("Pucu","Seer");

        member.addChildMember(puca);
        member.addChildMember(puce);
        member.addChildMember(puco);
        member.addChildMember(puci);

        assertEquals(member.getChildMembers().size(),3);
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member guildMaster = new PremiumMember("Obel","Master");

        Member puci = new OrdinaryMember("Pucu","Seer");
        Member puca = new OrdinaryMember("Pucu","Seer");
        Member puco = new OrdinaryMember("Pucu","Seer");
        Member puce = new OrdinaryMember("Pucu","Seer");

        guildMaster.addChildMember(puca);
        guildMaster.addChildMember(puce);
        guildMaster.addChildMember(puco);
        guildMaster.addChildMember(puci);

        assertEquals(guildMaster.getChildMembers().size(),4);
    }
}
