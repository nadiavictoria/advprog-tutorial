package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RawUpgradeTest {

    private RawUpgrade rawUpgrade;

    @BeforeEach
    public void setUp(){
        rawUpgrade = new RawUpgrade(new Shield());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals(rawUpgrade.getName(),"Shield");
    }

    @Test
    public void testMethodGetDescription(){
        //TODO: Complete me
        String sub = rawUpgrade.getDescription().substring(rawUpgrade.getDescription().length()-11);
        assertEquals("Raw Upgrade", sub);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int rawVal = rawUpgrade.getWeaponValue();
        int weapVal = rawUpgrade.weapon.getWeaponValue();
        boolean bool = (rawVal==weapVal+5)||(rawVal==weapVal+6)||(rawVal==weapVal+7)||(rawVal==weapVal+8)||(rawVal==weapVal+9)||(rawVal==weapVal+10);
        assertTrue(bool);
    }
}
